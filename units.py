from numbers import Number


class Milimeter(Number):
    pass


class Radian(Number):
    pass


class Second(Number):
    pass


class MilimeterPerSecond(Number):
    pass


class RadianPerSecond(Number):
    pass
