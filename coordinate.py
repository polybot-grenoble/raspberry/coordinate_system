from numbers import Number

from .units import *


class Vector2D:
    def __init__(self, x: Number, y: Number) -> None:
        self.x = x
        self.y = y

    def __mul__(self, other):
        if isinstance(other, Number):
            return Vector2D(self.x * other, self.y * other)
        else:
            raise ValueError(f"Un-implemented type for multiplication yet \"{type(other)}\"")

    def __add__(self, other: 'Vector2D') -> 'Vector2D':
        return Vector2D(self.x + other.x, self.y + other.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))

    def __str__(self) -> str:
        return f"[{self.x}, {self.y}]"
    def __repr__(self) -> str:
        return str(self)


class Vector2DAndRotation:
    def __init__(self, x: Number, y: Number, teta: Number) -> None:
        self.x_y = Vector2D(x, y)
        self.teta = teta

    @classmethod
    def from_vector(cls, x_y: Vector2D, teta: Number) -> "Vector2DAndRotation":
        return cls(x_y.x, x_y.y, teta)

    @property
    def x(self) -> Number:
        return self.x_y.x
    @x.setter
    def x(self, x: Number) -> None:
        self.x_y.x = x
    @property
    def y(self) -> Number:
        return self.x_y.y
    @y.setter
    def y(self, y: Number) -> None:
        self.x_y.y = y

    def __mul__(self, other):
        if isinstance(other, Number):
            return Vector2DAndRotation.from_vector(self.x_y * other, self.teta)
        else:
            raise ValueError(f"Un-implemented type for multiplication yet \"{type(other)}\"")

    def __str__(self) -> str:
        return f"({self.x_y}, {self.teta})"
    def __repr__(self) -> str:
        return str(self)


class Position(Vector2DAndRotation):
    pass


class Speed(Vector2DAndRotation):
    pass
